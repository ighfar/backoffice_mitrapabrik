const state = () => ({
    id:null,
    level:null,
    nama:null,
});

const getters = {
    getIdAdmin: (state) => state.id,
    getLevelAdmin: (state) => state.level,
    getNamaAdmin: (state) => state.nama,
};

const actions = {
    adminLogin({ commit }, {id, level, nama}) {
        commit('setAdminLogin',{id, level, nama});
    },
    adminLogout({ commit }) {
        commit('logout')
    },
};

const mutations = {
    setAdminLogin: (state, {id, level, nama}) => {
        state.id = id;
        state.level = level;
        state.nama = nama;
    },
    logout: (state) => {
        state.id = null
        state.level = null
        state.nama = null
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}