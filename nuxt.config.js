export default {
  server: {     
    port: 7000, // default: 3000     
  }, 
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'mitrapabrik-backoffice',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css' }
    ]
  },

  env: {
    // API_URL: 'http://localhost:4000/api',
    // BASE_BACKEND_URL:'http://localhost:4000',

    API_URL: 'https://api.mitrapabrik.com/api',
    BASE_BACKEND_URL:'https://api.mitrapabrik.com',
  },
  // server: {     
  //   port: 7000, // default: 3000     
  //   host: '0.0.0.0', // default: localhost   
  // },  
  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~/assets/css/main-theme.scss'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/decodeHTML.js',
    '~/plugins/formatDate.js',
    '~/plugins/formatCurrency.js', 
    '~/plugins/getUmur.js', 
    '~/plugins/formatIDDate.js', 
    '~/plugins/timeAgo.js', 
    {src:'~/plugins/qr.js',mode: 'client'}
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/pwa',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    'bootstrap-vue/nuxt',
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    'cookie-universal-nuxt',
    '@nuxtjs/markdownit'
  ],

  markdownit: {
    // preset: 'zero',
    // html: true,
    // linkify: true,
    // breaks: true,
    runtime: true,
  },

  bootstrapVue: {
    icons: true
  },
  
  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // baseURL:'http://localhost:4000/api',
    baseURL:'https://api.mitrapabrik.com/api',
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en'
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
