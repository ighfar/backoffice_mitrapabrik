exports.ids = [16];
exports.modules = {

/***/ 63:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/logout.vue?vue&type=template&id=744d3628&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c("div")}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/logout.vue?vue&type=template&id=744d3628&

// EXTERNAL MODULE: external "vuex"
var external_vuex_ = __webpack_require__(6);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/logout.vue?vue&type=script&lang=js&
//
//
//

/* harmony default export */ var logoutvue_type_script_lang_js_ = ({
  methods: { ...Object(external_vuex_["mapActions"])({
      'adminLogout': 'auth/adminLogout'
    })
  },

  mounted() {
    this.$cookies.removeAll();
    this.adminLogout();
    this.$router.push({
      name: 'index'
    });
  }

});
// CONCATENATED MODULE: ./pages/logout.vue?vue&type=script&lang=js&
 /* harmony default export */ var pages_logoutvue_type_script_lang_js_ = (logoutvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./pages/logout.vue





/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  pages_logoutvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "4a27d4b2"
  
)

/* harmony default export */ var logout = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=logout.js.map