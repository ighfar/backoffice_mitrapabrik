exports.ids = [15];
exports.modules = {

/***/ 64:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/index.vue?vue&type=template&id=63a0d440&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container-fluid d-flex justify-content-center align-items-center bg-dark",staticStyle:{"min-height":"100vh"}},[_vm._ssrNode("<div class=\"row\">","</div>",[_vm._ssrNode("<div class=\"col-12 text-center text-white my-3\"><h3>Login</h3></div> "),_vm._ssrNode("<div class=\"col-12\">","</div>",[_c('b-card',{staticClass:"mb-2"},[_c('b-form',{on:{"submit":function($event){$event.preventDefault();return _vm.login.apply(null, arguments)}}},[_c('b-form-group',{attrs:{"id":"input-group-1","label":"Username:","label-for":"input-1"}},[_c('b-form-input',{attrs:{"id":"input-1","placeholder":"Username","state":_vm.validation.id_admin},model:{value:(_vm.form.id_admin),callback:function ($$v) {_vm.$set(_vm.form, "id_admin", $$v)},expression:"form.id_admin"}}),_vm._v(" "),_c('b-form-invalid-feedback',{attrs:{"state":_vm.validation.id_admin}},[_vm._v("\n                        Username tidak valid\n                    ")])],1),_vm._v(" "),_c('b-form-group',{attrs:{"id":"input-group-2","label":"Password:","label-for":"input-2"}},[_c('b-form-input',{attrs:{"id":"input-2","type":"password","placeholder":"Password","state":_vm.validation.password},model:{value:(_vm.form.password),callback:function ($$v) {_vm.$set(_vm.form, "password", $$v)},expression:"form.password"}}),_vm._v(" "),_c('b-form-invalid-feedback',{attrs:{"state":_vm.validation.password}},[_vm._v("\n                        password tidak valid\n                    ")])],1),_vm._v(" "),_c('b-button',{staticClass:"mb-3",attrs:{"variant":"primary","block":"","type":"submit","disabled":_vm.loading}},[_vm._v("Login")]),_vm._v(" "),_c('b-link',{staticClass:"text-center"},[_c('small',[_vm._v("Forgot Password? Click here")])])],1)],1)],1)],2)])}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/index.vue?vue&type=template&id=63a0d440&

// EXTERNAL MODULE: external "vuex"
var external_vuex_ = __webpack_require__(6);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/index.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var lib_vue_loader_options_pagesvue_type_script_lang_js_ = ({
  middleware: 'landing',

  data() {
    return {
      form: {
        id_admin: '',
        password: ''
      },
      validation: {
        id_admin: null,
        password: null
      },
      loading: false
    };
  },

  methods: { ...Object(external_vuex_["mapActions"])({
      'adminLogin': 'auth/adminLogin'
    }),

    async login() {
      this.loading = true;

      for (const key in this.validation) {
        this.validation[key] = null;
      }

      await this.$axios.post('/user/admin/login', this.form).then(res => {
        this.adminLogin(res.data.data);
        this.$cookies.set('admin-token', res.data.token);
        this.$router.push({
          name: 'home'
        });
      }).catch(err => {
        if (err.response) err.response.data.errors.forEach(row => {
          this.validation[row.param] = false;
        });
      });
      this.loading = false;
    }

  }
});
// CONCATENATED MODULE: ./pages/index.vue?vue&type=script&lang=js&
 /* harmony default export */ var pagesvue_type_script_lang_js_ = (lib_vue_loader_options_pagesvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./pages/index.vue





/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  pagesvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "3fd5c976"
  
)

/* harmony default export */ var pages = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=index.js.map