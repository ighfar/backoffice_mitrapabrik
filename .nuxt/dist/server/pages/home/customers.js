exports.ids = [5];
exports.modules = {

/***/ 40:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(45);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("ae8817de", content, true, context)
};

/***/ }),

/***/ 44:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_customers_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(40);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_customers_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_customers_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_customers_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_customers_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 45:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".hidden_header{display:none}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 53:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/home/customers.vue?vue&type=template&id=8db45818&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container-fluid",staticStyle:{"max-height":"100vh","overflow":"auto"}},[_vm._ssrNode("<div class=\"row mt-3\"><div class=\"col-12\"><h1>Customers</h1></div></div> "),_vm._ssrNode("<div class=\"row\">","</div>",[_vm._ssrNode("<div class=\"col-md-6\">","</div>",[_vm._ssrNode("<label for=\"filter-input\">Omni-Search</label> "),_c('b-input-group',[_c('b-form-input',{attrs:{"id":"filter-input","type":"search","placeholder":"Type to Search","debounce":"500"},on:{"change":function($event){return _vm.fetchTableData()}},model:{value:(_vm.filter),callback:function ($$v) {_vm.filter=$$v},expression:"filter"}}),_vm._v(" "),_c('b-input-group-append',[_c('b-button',{attrs:{"disabled":!_vm.filter},on:{"click":_vm.clearOmniSearch}},[_vm._v("Clear")])],1)],1)],2),_vm._ssrNode(" "),_c('b-table',{attrs:{"items":_vm.items,"fields":['action'],"thead-class":"hidden_header","current-page":_vm.currentPage,"per-page":0,"borderless":""},scopedSlots:_vm._u([{key:"cell(action)",fn:function(row){return [_c('b-card',{staticClass:"h-100",scopedSlots:_vm._u([{key:"footer",fn:function(){return [_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-3 d-flex justify-content-center align-self-center"},[_c('b-avatar',{attrs:{"rounded":"","src":_vm.baseUrl+'/company_logo/'+row.item.foto_perusahaan,"size":"5rem"}})],1),_vm._v(" "),_c('div',{staticClass:"col-9 p-0"},[_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-md-4 font-weight-bold"},[_vm._v("Nama Perusahaan")]),_vm._v(" "),_c('div',{staticClass:"col-md-8"},[_vm._v(_vm._s(_vm.$decodeHtml(row.item.nama_perusahaan)))])]),_vm._v(" "),_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-md-4 font-weight-bold"},[_vm._v("Kategori Perusahaan")]),_vm._v(" "),_c('div',{staticClass:"col-md-8"},[_vm._v(_vm._s(row.item.kategori_perusahaan))])]),_vm._v(" "),_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-md-4 font-weight-bold"},[_vm._v("Alamat Perusahaan")]),_vm._v(" "),_c('div',{staticClass:"col-md-8"},[_vm._v(_vm._s(_vm.$decodeHtml(row.item.alamat_perusahaan)))])])])])]},proxy:true}],null,true)},[_c('b-badge',{staticClass:"position-absolute",staticStyle:{"top":"10px","right":"10px"},attrs:{"variant":"primary"}},[_vm._v("ID: "+_vm._s(row.item.id_user))]),_vm._v(" "),_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-md-3"},[_c('p',{staticClass:"text-center"},[_c('b-avatar',{attrs:{"src":_vm.baseUrl+'/profile/'+row.item.foto_profil,"size":"6rem"}})],1),_vm._v(" "),_c('b-button',{attrs:{"size":"sm","variant":"primary","pill":"","block":""},on:{"click":function($event){return _vm.getCustomerRequests(row.item.id_user)}}},[_vm._v("List Request")])],1),_vm._v(" "),_c('div',{staticClass:"col-md-9"},[_c('span',{staticClass:"font-weight-bold m-0"},[_vm._v(_vm._s(row.item.nama_depan)+" "+_vm._s(row.item.nama_belakang))]),_vm._v(" "),_c('span',{staticClass:"font-italic"},[_vm._v(_vm._s(row.item.role)+" - ("+_vm._s(row.item.profesi)+")")]),_vm._v(" "),_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-12"},[_vm._v(_vm._s(row.item.email))])]),_vm._v(" "),_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-4"},[_vm._v("No. HP")]),_vm._v(" "),_c('div',{staticClass:"col-8"},[_vm._v(": "+_vm._s(row.item.no_hp))])]),_vm._v(" "),_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-4"},[_vm._v("Minat")]),_vm._v(" "),_c('div',{staticClass:"col-8"},[_vm._v(": "+_vm._s(row.item.minat))])]),_vm._v(" "),_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-4"},[_vm._v("Asal Kota")]),_vm._v(" "),_c('div',{staticClass:"col-8"},[_vm._v(": "+_vm._s(row.item.asal_kota))])]),_vm._v(" "),_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-4"},[_vm._v("Tujuan")]),_vm._v(" "),_c('div',{staticClass:"col-8"},[_vm._v(": "+_vm._s(row.item.tujuan))])])])])],1)]}}])}),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"col-12\">","</div>",[(!_vm.loading)?_c('b-pagination',{staticClass:"my-3",attrs:{"total-rows":_vm.totalRows,"per-page":_vm.perPage,"align":"right","size":"sm"},model:{value:(_vm.currentPage),callback:function ($$v) {_vm.currentPage=$$v},expression:"currentPage"}}):_vm._e()],1)],2),_vm._ssrNode(" "),_c('b-modal',{attrs:{"centered":"","content-class":"shadow","visible":_vm.showAlert,"hide-footer":"","hide-header":""}},[_c('p',{staticClass:"my-2 text-center"},[_vm._v("Apakah anda yakin hendak menghapus data ini?")]),_vm._v(" "),_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-md-12"},[_c('b-button',{staticClass:"w-25 float-right mx-2",attrs:{"variant":"primary","pill":""},on:{"click":_vm.deleteData}},[_vm._v("Ya")]),_vm._v(" "),_c('b-button',{staticClass:"w-25 float-right mx-2",attrs:{"variant":"light","pill":""},on:{"click":function($event){_vm.showAlert = !_vm.showAlert}}},[_vm._v("Tidak")])],1)])]),_vm._ssrNode(" "),_c('b-modal',{attrs:{"centered":"","content-class":"shadow","hide-footer":"","hide-header":"","size":"lg"},model:{value:(_vm.showRequestAlert),callback:function ($$v) {_vm.showRequestAlert=$$v},expression:"showRequestAlert"}},[_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-12"},[_c('h5',[_vm._v("List Request")])])]),_vm._v(" "),(_vm.selectedData.length <= 0)?_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-12"},[_c('h6',{staticClass:"text-secondary"},[_vm._v("belum mengajukan request.")])])]):_vm._l((_vm.selectedData),function(req,idx){return _c('div',{key:idx,staticClass:"card p-3 bg-light mb-3"},[_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-6"},[_c('span',{staticClass:"font-weight-bold"},[_vm._v("Tgl pengajuan request")]),_vm._v(" "),_c('p',[_vm._v(_vm._s(_vm.$formatIDDate(req.timestamp)))]),_vm._v(" "),_c('span',{staticClass:"font-weight-bold"},[_vm._v("Brand")]),_vm._v(" "),_c('p',[_vm._v(_vm._s(req.brand))]),_vm._v(" "),_c('span',{staticClass:"font-weight-bold"},[_vm._v("Material Qty.")]),_vm._v(" "),_c('p',[_vm._v(_vm._s(req.material_qty))])]),_vm._v(" "),_c('div',{staticClass:"col-6"},[_c('span',{staticClass:"font-weight-bold"},[_vm._v("Lokasi")]),_vm._v(" "),_c('p',[_vm._v(_vm._s(req.lokasi))]),_vm._v(" "),_c('span',{staticClass:"font-weight-bold"},[_vm._v("Kebutuhan")]),_vm._v(" "),_c('p',[_vm._v(_vm._s(req.kebutuhan))])])])])}),_vm._v(" "),_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-md-12"},[_c('b-button',{staticClass:"w-25 float-right mx-2",attrs:{"variant":"light","pill":""},on:{"click":function($event){_vm.showRequestAlert = false}}},[_vm._v("Close")])],1)])],2)],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/home/customers.vue?vue&type=template&id=8db45818&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/home/customers.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var customersvue_type_script_lang_js_ = ({
  data() {
    return {
      baseUrl: "https://api.mitrapabrik.com",
      showAlert: false,
      showRequestAlert: false,
      loading: false,
      items: [],
      selectedData: [],
      fields: [{
        key: 'actions',
        sortable: false
      }],
      currentPage: 1,
      totalRows: 0,
      perPage: 10,
      filter: '',
      tmpData: {}
    };
  },

  methods: {
    async fetchTableData() {
      let listCustomers = await this.$axios.get(`/user`, {
        params: {
          limit: this.perPage,
          offset: (this.currentPage - 1) * this.perPage,
          omni: this.filter
        },
        headers: {
          'auth-token': this.$cookies.get('admin-token')
        }
      });
      this.totalRows = listCustomers.data.data[0] && listCustomers.data.data[0].total_rows || 0;
      this.items = listCustomers.data.data;
    },

    async clearOmniSearch() {
      this.filter = '';
      await this.fetchTableData();
    },

    async getCustomerRequests(id_user) {
      let fetchRequest = await this.$axios.get(`/requests/${id_user}`, {
        params: {},
        headers: {
          'auth-token': this.$cookies.get('admin-token')
        }
      });
      this.selectedData = fetchRequest.data.data;
      this.showRequestAlert = true;
    },

    onFiltered(filteredItems) {
      this.totalRows = filteredItems.length;
      this.currentPage = 1;
    },

    openInsertForm() {
      localStorage.removeItem('payload-edit');
      this.$router.push({
        name: 'home-mutations-artikel-form'
      });
    },

    openEditForm(row) {
      localStorage.setItem('payload-edit', JSON.stringify(row.item.item));
      this.$router.push({
        name: 'home-mutations-artikel-form'
      });
    },

    async publishArtikel(row) {
      await this.$axios.put(`/artikels/publish`, {
        id_artikel: row.item.item.id_artikel,
        status: row.item.item.status == 1 ? 0 : 1
      }, {
        headers: {
          "auth-token": this.$cookies.get('admin-token')
        }
      }).then(async res => {
        this.$bvToast.toast(`Artikel status updated.`, {
          title: 'Info',
          autoHideDelay: 5000,
          solid: true
        });
        await this.fetchTableData();
      }).catch(err => {
        this.$bvToast.toast(`Mohon maaf terjadi kesalahan. Silahkan coba beberapa saat lagi.`, {
          title: 'Error',
          autoHideDelay: 5000,
          solid: true
        });
      });
    },

    openDeleteAlert(row) {
      this.showAlert = true;
      this.tmpData = row;
    },

    async deleteData() {
      await this.$axios.delete(`/artikels`, {
        headers: {
          "auth-token": this.$cookies.get('admin-token')
        },
        data: {
          id_artikel: this.tmpData.item.id_artikel
        }
      }).then(async res => {
        this.$bvToast.toast(`Artikel deleted.`, {
          title: 'Info',
          autoHideDelay: 5000,
          solid: true
        });
        await this.fetchTableData();
      }).catch(err => {
        console.log(err.response);
      });
      this.showAlert = false;
    }

  },

  async mounted() {
    await this.fetchTableData();
    this.loading = false;
  }

});
// CONCATENATED MODULE: ./pages/home/customers.vue?vue&type=script&lang=js&
 /* harmony default export */ var home_customersvue_type_script_lang_js_ = (customersvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./pages/home/customers.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(44)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  home_customersvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "7083c1b5"
  
)

/* harmony default export */ var customers = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=customers.js.map