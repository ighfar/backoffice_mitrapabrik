exports.ids = [13];
exports.modules = {

/***/ 42:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(49);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("b49f3d78", content, true, context)
};

/***/ }),

/***/ 48:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_scan_qr_vue_vue_type_style_index_0_id_2ad9ae9e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(42);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_scan_qr_vue_vue_type_style_index_0_id_2ad9ae9e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_scan_qr_vue_vue_type_style_index_0_id_2ad9ae9e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_scan_qr_vue_vue_type_style_index_0_id_2ad9ae9e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_scan_qr_vue_vue_type_style_index_0_id_2ad9ae9e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 49:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".scan-confirmation[data-v-2ad9ae9e]{position:absolute;width:100%;height:100%;background-color:hsla(0,0%,100%,.8);display:flex;flex-flow:row nowrap;justify-content:center}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 60:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/home/scan-qr.vue?vue&type=template&id=2ad9ae9e&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container-fluid vh-100"},[_vm._ssrNode("<div class=\"row\" data-v-2ad9ae9e>","</div>",[_vm._ssrNode("<div class=\"col-12\" data-v-2ad9ae9e>","</div>",[_vm._ssrNode("<p class=\"error\" data-v-2ad9ae9e>"+_vm._ssrEscape(_vm._s(_vm.error))+"</p> "),_c('qrcode-stream',{attrs:{"camera":_vm.camera},on:{"decode":_vm.onDecode,"init":_vm.onInit}},[_c('div',{directives:[{name:"show",rawName:"v-show",value:(_vm.showScanConfirmation),expression:"showScanConfirmation"}],staticClass:"scan-confirmation"},[_c('img',{attrs:{"src":"https://cdn.pixabay.com/photo/2016/03/31/14/37/check-mark-1292787_960_720.png","alt":"Checkmark","width":"128px"}})])])],2)]),_vm._ssrNode(" "),_c('b-modal',{attrs:{"centered":"","content-class":"shadow","visible":_vm.showAlert,"hide-footer":"","hide-header":""}},[_c('h3',{staticClass:"my-2 text-center"},[_vm._v("QR Code berhasil di scan!")]),_vm._v(" "),(_vm.result)?_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-12 mb-3"},[_vm._v("\n                Recipient details: "),_c('br'),_vm._v(" "),_c('b',[_vm._v("transaction ID:")]),_vm._v(" "+_vm._s(_vm.result.id_trans)+"\n            ")]),_vm._v(" "),_c('div',{staticClass:"col-6"},[_c('span',{staticClass:"font-weight-bold"},[_vm._v("Email")]),_vm._v(" "),_c('p',[_vm._v(_vm._s(_vm.result.email))]),_vm._v(" "),_c('span',{staticClass:"font-weight-bold"},[_vm._v("Nama Lengkap")]),_vm._v(" "),_c('p',[_vm._v(_vm._s(_vm.result.nama_depan)+" "+_vm._s(_vm.result.nama_belakang))]),_vm._v(" "),_c('span',{staticClass:"font-weight-bold"},[_vm._v("No. HP")]),_vm._v(" "),_c('p',[_vm._v(_vm._s(_vm.result.no_hp))])]),_vm._v(" "),_c('div',{staticClass:"col-6"},[_c('span',{staticClass:"font-weight-bold"},[_vm._v("Event Name")]),_vm._v(" "),_c('p',[_vm._v(_vm._s(_vm.result.event_name))]),_vm._v(" "),_c('span',{staticClass:"font-weight-bold"},[_vm._v("Tgl Event")]),_vm._v(" "),_c('p',[_vm._v(_vm._s(_vm.$formatIDDate(_vm.result.starting_date)))])])]):_vm._e(),_vm._v(" "),_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-md-12"},[_c('b-button',{attrs:{"variant":"primary","pill":"","block":""},on:{"click":function($event){_vm.showAlert = false}}},[_vm._v("Ya")])],1)])])],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/home/scan-qr.vue?vue&type=template&id=2ad9ae9e&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/home/scan-qr.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var scan_qrvue_type_script_lang_js_ = ({
  data() {
    return {
      showAlert: false,
      camera: 'auto',
      result: null,
      error: '',
      showScanConfirmation: false
    };
  },

  methods: {
    async onDecode(decodedUrl) {
      this.camera = 'off'; // await this.axios.post(decodedUrl)

      await this.$axios.put(decodedUrl, {}, {
        headers: {
          "auth-token": this.$cookies.get('admin-token')
        }
      }).then(res => {
        this.showAlert = true;
        this.result = res.data.data[0];
      }).catch(err => {
        console.log(err);
      }); // await this.$axios.put('/events/attend/e3762f10-4b46-11ec-9bdd-7b415e82d84c', {}, { headers: {
      //     "auth-token":this.$cookies.get('admin-token')
      // }})
      // .then(res => {
      //     this.showAlert = true
      //     this.result = res.data.data[0]
      // })
      // .catch(err => {
      //     console.log(err)
      // })

      this.camera = 'auto';
    },

    async onInit(promise) {
      try {
        await promise;
      } catch (error) {
        if (error.name === 'NotAllowedError') {
          this.error = "ERROR: you need to grant camera access permission";
        } else if (error.name === 'NotFoundError') {
          this.error = "ERROR: no camera on this device";
        } else if (error.name === 'NotSupportedError') {
          this.error = "ERROR: secure context required (HTTPS, localhost)";
        } else if (error.name === 'NotReadableError') {
          this.error = "ERROR: is the camera already in use?";
        } else if (error.name === 'OverconstrainedError') {
          this.error = "ERROR: installed cameras are not suitable";
        } else if (error.name === 'StreamApiNotSupportedError') {
          this.error = "ERROR: Stream API is not supported in this browser";
        } else if (error.name === 'InsecureContextError') {
          this.error = 'ERROR: Camera access is only permitted in secure context. Use HTTPS or localhost rather than HTTP.';
        } else {
          this.error = `ERROR: Camera error (${error.name})`;
        }
      }
    }

  }
});
// CONCATENATED MODULE: ./pages/home/scan-qr.vue?vue&type=script&lang=js&
 /* harmony default export */ var home_scan_qrvue_type_script_lang_js_ = (scan_qrvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./pages/home/scan-qr.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(48)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  home_scan_qrvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "2ad9ae9e",
  "44cb0cd1"
  
)

/* harmony default export */ var scan_qr = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=scan-qr.js.map