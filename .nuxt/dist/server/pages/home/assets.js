exports.ids = [4];
exports.modules = {

/***/ 52:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/home/assets.vue?vue&type=template&id=57f7828c&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container-fluid mx-4"},[_vm._ssrNode("<div class=\"row mt-3\" data-v-57f7828c><div class=\"col-12\" data-v-57f7828c><h1 data-v-57f7828c>Assets</h1></div></div> "),_vm._ssrNode("<div class=\"row\" data-v-57f7828c>","</div>",[_vm._ssrNode("<div class=\"col-12 mb-3\" data-v-57f7828c>","</div>",[_c('b-button',{staticClass:"px-5",attrs:{"variant":"primary","pill":"","size":"sm"},on:{"click":function($event){_vm.showUploadForm = true}}},[_vm._v("+ Upload asset")])],1)]),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"row\" data-v-57f7828c>","</div>",[_vm._ssrNode("<div class=\"col-12\" data-v-57f7828c>","</div>",[_vm._ssrNode("<div class=\"row mt-3\" data-v-57f7828c>","</div>",[_vm._ssrNode("<div class=\"col-12\" data-v-57f7828c><h3 data-v-57f7828c>Asset Anda</h3></div> "),_vm._l((_vm.tempUrl),function(tmp,idx){return _vm._ssrNode("<div class=\"col-auto d-flex justify-content-center align-items-center\" data-v-57f7828c>","</div>",[_vm._ssrNode("<div class=\"p-3 card h-100\" data-v-57f7828c>","</div>",[_c('b-img',{staticClass:"shadow mb-5",attrs:{"src":tmp,"fluid":"","width":"150","height":"150"}}),_vm._ssrNode(" "),_c('b-button',{staticClass:"px-5 mt-2 position-absolute",staticStyle:{"bottom":"15px"},attrs:{"variant":"secondary","pill":"","size":"sm"},on:{"click":function($event){return _vm.deleteGallery(idx)}}},[_vm._v("Delete")])],2)])})],2)]),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"row\" data-v-57f7828c>","</div>",_vm._l((_vm.assets),function(tmp,idx){return _vm._ssrNode("<div class=\"col-auto d-flex justify-content-center align-items-center\" data-v-57f7828c>","</div>",[_vm._ssrNode("<div class=\"p-3 card h-100\" data-v-57f7828c>","</div>",[_c('b-img',{staticClass:"shadow mb-5",attrs:{"src":tmp,"fluid":"","width":"150","height":"150"}}),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"row position-absolute\" style=\"bottom: 10px;\" data-v-57f7828c>","</div>",[_vm._ssrNode("<div class=\"col-12\" data-v-57f7828c>","</div>",[_c('b-button',{directives:[{name:"b-tooltip",rawName:"v-b-tooltip.hover",modifiers:{"hover":true}}],staticClass:"mt-2",attrs:{"variant":"secondary","size":"sm","title":"Delete Asset"},on:{"click":function($event){return _vm.deleteGallery(tmp)}}},[_c('b-icon',{attrs:{"icon":"trash-fill"}})],1),_vm._ssrNode(" "),_c('b-button',{directives:[{name:"b-tooltip",rawName:"v-b-tooltip.hover",modifiers:{"hover":true}}],staticClass:"mt-2",attrs:{"variant":"success","size":"sm","title":_vm.tooltipTitle},on:{"click":function($event){return _vm.copyText(tmp)}}},[_c('b-icon',{attrs:{"icon":"link45deg"}})],1)],2)])],2)])}),0)],2),_vm._ssrNode(" "),_c('b-modal',{attrs:{"centered":"","content-class":"shadow","hide-footer":"","hide-header":"","size":"lg"},model:{value:(_vm.showUploadForm),callback:function ($$v) {_vm.showUploadForm=$$v},expression:"showUploadForm"}},[_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-md-12"},[_c('b-form-group',{attrs:{"id":"input-group-gallery","label":"Upload Gallery","label-for":"input-gallery"}},[_c('b-form-file',{staticClass:"rounded-pill bg-glass",attrs:{"accept":"image/jpeg, image/png, image/jiff","id":"input-gallery","multiple":""},on:{"input":_vm.uploadAssets},model:{value:(_vm.assets),callback:function ($$v) {_vm.assets=$$v},expression:"assets"}})],1),_vm._v(" "),_c('div',{staticClass:"row"},_vm._l((_vm.tempUploadThumbnail),function(tmp,idx){return _c('div',{key:idx,staticClass:"col-auto d-flex justify-content-center align-items-center"},[_c('div',{staticClass:"p-3 card h-100"},[_c('b-img',{staticClass:"shadow mb-5",attrs:{"src":tmp,"fluid":"","width":"150","height":"150"}}),_vm._v(" "),_c('b-button',{attrs:{"variant":"dark","pill":"","size":"sm"},on:{"click":function($event){return _vm.removeFile(idx)}}},[_vm._v("X remove")])],1)])}),0)],1),_vm._v(" "),_c('div',{staticClass:"col-12"},[_c('b-button',{staticClass:"px-5 float-right",attrs:{"variant":"primary","pill":""},on:{"click":_vm.submitAsset}},[_vm._v("Upload Asset")]),_vm._v(" "),_c('b-button',{staticClass:"w-25 float-right mx-2",attrs:{"variant":"light","pill":""},on:{"click":function($event){_vm.showUploadForm = false}}},[_vm._v("Close")])],1)])]),_vm._ssrNode(" "),_c('b-modal',{attrs:{"centered":"","content-class":"shadow","visible":_vm.showAlert,"hide-footer":"","hide-header":""}},[_c('p',{staticClass:"my-2 text-center"},[_vm._v(_vm._s(_vm.alertMsg))]),_vm._v(" "),_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-md-12"},[_c('b-button',{staticClass:"w-25 float-right mx-2",attrs:{"variant":"primary","pill":""},on:{"click":function($event){_vm.showAlert = false}}},[_vm._v("OK")])],1)])])],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/home/assets.vue?vue&type=template&id=57f7828c&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/home/assets.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var assetsvue_type_script_lang_js_ = ({
  data() {
    return {
      baseUrl: "https://api.mitrapabrik.com",
      showUploadForm: false,
      assets: [],
      tempUploadThumbnail: [],
      tempUrl: [],
      showAlert: false,
      alertMsg: '',
      tooltipTitle: 'Get Link'
    };
  },

  methods: {
    uploadAssets() {
      if (this.assets.length > 0) {
        this.assets.forEach(row => {
          this.tempUploadThumbnail.push(window.URL.createObjectURL(row));
        });
      }
    },

    copyText(link) {
      this.tooltipTitle = 'Link Copied!';
      navigator.clipboard.writeText(link);
      setTimeout(() => {
        this.tooltipTitle = 'Get Link';
      }, 1000);
    },

    removeFile(idx) {
      this.tempUploadThumbnail.splice(idx, 1);
      this.assets.splice(idx, 1);
    },

    async submitAsset() {
      let formData = new FormData();
      this.assets.forEach(img => {
        formData.append('gambar', img);
      });
      await this.$axios.post(`/artikels/asset/upload`, formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
          "auth-token": this.$cookies.get('admin-token')
        }
      }).then(async res => {
        this.showAlert = true;
        this.alertMsg = 'upload file berhasil!';
        this.tempUploadThumbnail = [];
        this.assets = [];
        await this.fetchAssets();
      }).catch(err => {
        this.showAlert = true;
        this.alertMsg = 'mohon maaf, terjadi kesalahan, silahkan coba beberapa saat lagi.';
        console.log(err.response);
      });
    },

    async deleteGallery(id_asset) {
      let id = id_asset.split('/');
      await this.$axios.delete(`/artikels/asset/${id[4]}`, {
        headers: {
          "auth-token": this.$cookies.get('admin-token')
        },
        data: {}
      }).then(async res => {
        this.showAlert = true;
        this.alertMsg = 'delete gambar berhasil!';
        this.tempUploadThumbnail = [];
        this.assets = [];
        await this.fetchAssets();
      }).catch(err => {
        this.showAlert = true;
        this.alertMsg = 'mohon maaf, terjadi kesalahan, silahkan coba beberapa saat lagi.';
        console.log(err.response);
      });
    },

    async fetchAssets() {
      let assetNames = await this.$axios.get(`/artikels/asset`, {
        params: {},
        headers: {
          'auth-token': this.$cookies.get('admin-token')
        }
      });
      assetNames.data.data && assetNames.data.data.forEach(row => {
        this.assets.push(`${this.baseUrl}/asset/${row}`);
      });
    }

  },

  async mounted() {
    await this.fetchAssets();
  }

});
// CONCATENATED MODULE: ./pages/home/assets.vue?vue&type=script&lang=js&
 /* harmony default export */ var home_assetsvue_type_script_lang_js_ = (assetsvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./pages/home/assets.vue



function injectStyles (context) {
  
  
}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  home_assetsvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "57f7828c",
  "5aa7a75a"
  
)

/* harmony default export */ var assets = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=assets.js.map