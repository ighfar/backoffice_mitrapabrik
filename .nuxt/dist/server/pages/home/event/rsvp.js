exports.ids = [7];
exports.modules = {

/***/ 62:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/home/event/rsvp.vue?vue&type=template&id=3a36efc2&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container-fluid mx-4"},[(_vm.eventDetails)?[_vm._ssrNode("<div class=\"row mt-3\" data-v-3a36efc2><div class=\"col-12\" data-v-3a36efc2><h1 data-v-3a36efc2>"+_vm._ssrEscape("RSVP List for "+_vm._s(_vm.eventDetails.event_name))+"</h1></div> <div class=\"col-12\" data-v-3a36efc2><div class=\"row\" data-v-3a36efc2><div class=\"col-4 col-md-2\" data-v-3a36efc2>Tgl Event</div> <div class=\"col-8 col-md-10\" data-v-3a36efc2>"+_vm._ssrEscape(": "+_vm._s(_vm.$formatIDDate(_vm.eventDetails.starting_date)))+"</div> <div class=\"col-4 col-md-2\" data-v-3a36efc2>Author</div> <div class=\"col-8 col-md-10\" data-v-3a36efc2>"+_vm._ssrEscape(": "+_vm._s(_vm.eventDetails.author))+"</div> <div class=\"col-4 col-md-2\" data-v-3a36efc2>Lokasi Event</div> <div class=\"col-8 col-md-10\" data-v-3a36efc2>"+_vm._ssrEscape(": "+_vm._s(_vm.eventDetails.location))+"</div> <div class=\"col-4 col-md-2\" data-v-3a36efc2>Admission</div> <div class=\"col-8 col-md-10\" data-v-3a36efc2>"+_vm._ssrEscape(": "+_vm._s(_vm.eventDetails.admission))+"</div></div></div></div> "),_vm._ssrNode("<div class=\"row\" data-v-3a36efc2>","</div>",[_vm._ssrNode("<div class=\"col-12 p-0\" data-v-3a36efc2>","</div>",[(!_vm.loading)?_c('b-table',{attrs:{"items":_vm.items,"fields":_vm.fields,"responsive":"","head-variant":"dark","current-page":_vm.currentPage,"per-page":_vm.perPage,"filter":_vm.filter},on:{"filtered":_vm.onFiltered},scopedSlots:_vm._u([{key:"cell(Attendance_ID)",fn:function(row){return [_vm._v("\n                        "+_vm._s(row.item.id_trans)+"\n                    ")]}},{key:"cell(User_Details)",fn:function(row){return [_c('b',[_vm._v(_vm._s(row.item.nama_depan)+" "+_vm._s(row.item.nama_belakang))]),_vm._v(" - "),_c('em',[_vm._v(_vm._s(row.item.email))])]}},{key:"cell(attendance)",fn:function(row){return [(row.item.attendance == 1)?_c('p',{staticClass:"text-success font-weight-bold"},[_vm._v("HADIR")]):_c('p',{staticClass:"text-secondary font-weight-bold"},[_vm._v("Belum hadir")])]}},{key:"cell(timestamp)",fn:function(row){return [_vm._v("\n                        "+_vm._s(_vm.$formatIDDate(row.item.timestamp))+"\n                    ")]}}],null,false,62535681)}):_vm._e()],1),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"col-12\" data-v-3a36efc2>","</div>",[(!_vm.loading)?_c('b-pagination',{staticClass:"my-3",attrs:{"total-rows":_vm.totalRows,"per-page":_vm.perPage,"align":"right","size":"sm"},model:{value:(_vm.currentPage),callback:function ($$v) {_vm.currentPage=$$v},expression:"currentPage"}}):_vm._e()],1)],2)]:_vm._ssrNode("<div class=\"row mt-3\" data-v-3a36efc2><div class=\"col-12\" data-v-3a36efc2><h3 class=\"text-secondary\" data-v-3a36efc2>Belum ada yang mendaftar...</h3></div></div>")],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/home/event/rsvp.vue?vue&type=template&id=3a36efc2&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/home/event/rsvp.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var rsvpvue_type_script_lang_js_ = ({
  data() {
    return {
      loading: false,
      items: [],
      fields: [{
        key: 'Attendance_ID',
        sortable: false
      }, {
        key: 'id_event',
        sortable: true
      }, {
        key: 'timestamp',
        sortable: true
      }, {
        key: 'User_Details',
        sortable: true
      }, {
        key: 'attendance',
        sortable: true
      }],
      eventDetails: null,
      currentPage: 1,
      totalRows: 0,
      perPage: 10,
      filter: ''
    };
  },

  methods: {
    onFiltered(filteredItems) {
      this.totalRows = filteredItems.length;
      this.currentPage = 1;
    }

  },

  async mounted() {
    let listAttendance = await this.$axios.get(`/events/attendance/event/${this.$route.query.id}`, {
      params: {},
      headers: {
        'auth-token': this.$cookies.get('admin-token')
      }
    });
    this.items = listAttendance.data.data;
    this.totalRows = listAttendance.data.data.length;

    if (this.items.length > 0) {
      this.eventDetails = {
        event_name: this.items[0].event_name,
        starting_date: this.items[0].starting_date,
        author: this.items[0].author,
        location: this.items[0].location,
        admission: this.items[0].admission
      };
    }
  }

});
// CONCATENATED MODULE: ./pages/home/event/rsvp.vue?vue&type=script&lang=js&
 /* harmony default export */ var event_rsvpvue_type_script_lang_js_ = (rsvpvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./pages/home/event/rsvp.vue



function injectStyles (context) {
  
  
}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  event_rsvpvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "3a36efc2",
  "4c0ddee6"
  
)

/* harmony default export */ var rsvp = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=rsvp.js.map