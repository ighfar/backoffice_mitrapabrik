exports.ids = [3];
exports.modules = {

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/home/artikels.vue?vue&type=template&id=6db9f668&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container-fluid mx-4"},[_vm._ssrNode("<div class=\"row mt-3\" data-v-6db9f668><div class=\"col-12\" data-v-6db9f668><h1 data-v-6db9f668>Artikels</h1></div></div> "),_vm._ssrNode("<div class=\"row\" data-v-6db9f668>","</div>",[_vm._ssrNode("<div class=\"col-12 mb-3\" data-v-6db9f668>","</div>",[_c('b-button',{staticClass:"px-5",attrs:{"variant":"primary","pill":"","size":"sm"},on:{"click":_vm.openInsertForm}},[_vm._v("+ Add")])],1),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"col-12 p-0\" data-v-6db9f668>","</div>",[(!_vm.loading)?_c('b-table',{attrs:{"items":_vm.items,"fields":_vm.fields,"responsive":"","head-variant":"dark","current-page":_vm.currentPage,"per-page":_vm.perPage,"filter":_vm.filter},on:{"filtered":_vm.onFiltered},scopedSlots:_vm._u([{key:"cell(body)",fn:function(row){return [_c('p',{staticClass:"d-inline-block text-truncate",staticStyle:{"max-width":"150px"}},[_vm._v(_vm._s(row.item.body))])]}},{key:"cell(timestamp)",fn:function(row){return [_vm._v("\n                    "+_vm._s(_vm.$formatIDDate(row.item.timestamp))+"\n                ")]}},{key:"cell(status)",fn:function(row){return [_vm._v("\n                    "+_vm._s(row.item.status == 0 ? 'Unpublished.' : 'Published.')+"\n                ")]}},{key:"cell(action)",fn:function(row){return [_c('b-button',{staticClass:"mr-2",attrs:{"size":"sm","variant":"success","pill":"","block":""},on:{"click":function($event){return _vm.publishArtikel(row)}}},[_vm._v(_vm._s(row.item.status == 0 ? 'Publish' : 'Unpublish'))]),_vm._v(" "),_c('b-button',{staticClass:"mr-2",attrs:{"size":"sm","variant":"primary","pill":"","block":""},on:{"click":function($event){return _vm.openEditForm(row)}}},[_vm._v("Edit")]),_vm._v(" "),_c('b-button',{staticClass:"mr-2",attrs:{"size":"sm","variant":"dark","pill":"","block":""},on:{"click":function($event){return _vm.openDeleteAlert(row)}}},[_vm._v("Delete")])]}}],null,false,179127966)}):_vm._e()],1),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"col-12\" data-v-6db9f668>","</div>",[(!_vm.loading)?_c('b-pagination',{staticClass:"my-3",attrs:{"total-rows":_vm.totalRows,"per-page":_vm.perPage,"align":"right","size":"sm"},model:{value:(_vm.currentPage),callback:function ($$v) {_vm.currentPage=$$v},expression:"currentPage"}}):_vm._e()],1)],2),_vm._ssrNode(" "),_c('b-modal',{attrs:{"centered":"","content-class":"shadow","visible":_vm.showAlert,"hide-footer":"","hide-header":""}},[_c('p',{staticClass:"my-2 text-center"},[_vm._v("Apakah anda yakin hendak menghapus data ini?")]),_vm._v(" "),_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-md-12"},[_c('b-button',{staticClass:"w-25 float-right mx-2",attrs:{"variant":"primary","pill":""},on:{"click":_vm.deleteData}},[_vm._v("Ya")]),_vm._v(" "),_c('b-button',{staticClass:"w-25 float-right mx-2",attrs:{"variant":"light","pill":""},on:{"click":function($event){_vm.showAlert = !_vm.showAlert}}},[_vm._v("Tidak")])],1)])])],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/home/artikels.vue?vue&type=template&id=6db9f668&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/home/artikels.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var artikelsvue_type_script_lang_js_ = ({
  data() {
    return {
      showAlert: false,
      loading: false,
      items: [],
      fields: [{
        key: 'id_artikel',
        sortable: true
      }, {
        key: 'title',
        sortable: true
      }, {
        key: 'body',
        sortable: true
      }, {
        key: 'category',
        sortable: true
      }, {
        key: 'author',
        sortable: true
      }, {
        key: 'timestamp',
        sortable: true
      }, {
        key: 'tipe',
        sortable: true
      }, {
        key: 'status',
        sortable: false
      }, {
        key: 'action',
        sortable: false
      }],
      currentPage: 1,
      totalRows: 0,
      perPage: 10,
      filter: '',
      tmpData: {}
    };
  },

  methods: {
    async fetchTableData() {
      let listartikels = await this.$axios.get(`/artikels`);
      console.log(listartikels);
      this.totalRows = listartikels.data.data.length;
      this.items = listartikels.data.data;
    },

    onFiltered(filteredItems) {
      this.totalRows = filteredItems.length;
      this.currentPage = 1;
    },

    openInsertForm() {
      localStorage.removeItem('payload-edit');
      this.$router.push({
        name: 'home-mutations-artikel-form'
      });
    },

    openEditForm(row) {
      localStorage.setItem('payload-edit', JSON.stringify(row.item));
      this.$router.push({
        name: 'home-mutations-artikel-form'
      });
    },

    async publishArtikel(row) {
      await this.$axios.put(`/artikels/publish`, {
        id_artikel: row.item.id_artikel,
        status: row.item.status == 1 ? 0 : 1
      }, {
        headers: {
          "auth-token": this.$cookies.get('admin-token')
        }
      }).then(async res => {
        this.$bvToast.toast(`Artikel status updated.`, {
          title: 'Info',
          autoHideDelay: 5000,
          solid: true
        });
        await this.fetchTableData();
      }).catch(err => {
        this.$bvToast.toast(`Mohon maaf terjadi kesalahan. Silahkan coba beberapa saat lagi.`, {
          title: 'Error',
          autoHideDelay: 5000,
          solid: true
        });
      });
    },

    openDeleteAlert(row) {
      this.showAlert = true;
      this.tmpData = row;
    },

    async deleteData() {
      await this.$axios.delete(`/artikels`, {
        headers: {
          "auth-token": this.$cookies.get('admin-token')
        },
        data: {
          id_artikel: this.tmpData.item.id_artikel
        }
      }).then(async res => {
        this.$bvToast.toast(`Artikel deleted.`, {
          title: 'Info',
          autoHideDelay: 5000,
          solid: true
        });
        await this.fetchTableData();
      }).catch(err => {
        console.log(err.response);
      });
      this.showAlert = false;
    }

  },

  async mounted() {
    await this.fetchTableData();
    this.loading = false;
  }

});
// CONCATENATED MODULE: ./pages/home/artikels.vue?vue&type=script&lang=js&
 /* harmony default export */ var home_artikelsvue_type_script_lang_js_ = (artikelsvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./pages/home/artikels.vue



function injectStyles (context) {
  
  
}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  home_artikelsvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "6db9f668",
  "66c94497"
  
)

/* harmony default export */ var artikels = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=artikels.js.map