exports.ids = [12];
exports.modules = {

/***/ 41:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(47);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("2835fed6", content, true, context)
};

/***/ }),

/***/ 46:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_showroom_form_vue_vue_type_style_index_0_id_2337813a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(41);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_showroom_form_vue_vue_type_style_index_0_id_2337813a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_showroom_form_vue_vue_type_style_index_0_id_2337813a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_showroom_form_vue_vue_type_style_index_0_id_2337813a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_showroom_form_vue_vue_type_style_index_0_id_2337813a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 47:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".listbrand-container[data-v-2337813a]{max-height:200px;overflow-y:scroll}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 59:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/home/mutations/showroom-form.vue?vue&type=template&id=2337813a&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container-fluid mt-4"},[_vm._ssrNode("<div class=\"row\" data-v-2337813a><div class=\"col-12\" data-v-2337813a><h1 data-v-2337813a>Showroom Form</h1></div></div> "),_vm._ssrNode("<div class=\"row\" data-v-2337813a>","</div>",[_vm._ssrNode("<div class=\"col-md-12\" data-v-2337813a>","</div>",[_c('b-form-group',{attrs:{"id":"input-group-title","label":"title","label-for":"input-title"}},[_c('b-form-input',{attrs:{"id":"input-title","placeholder":"Masukkan title","state":_vm.validation.title},model:{value:(_vm.form.title),callback:function ($$v) {_vm.$set(_vm.form, "title", $$v)},expression:"form.title"}}),_vm._v(" "),_c('b-form-invalid-feedback',{attrs:{"state":_vm.validation.title}},[_vm._v("\n                    Title tidak valid\n                ")])],1)],1),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"col-md-4\" data-v-2337813a>","</div>",[_c('b-form-group',{attrs:{"id":"input-group-alamat","label":"Alamat showroom","label-for":"input-alamat"}},[_c('b-form-input',{staticClass:"mt-3",attrs:{"id":"input-alamat","state":_vm.validation.alamat,"placeholder":"Alamat lengkap showroom"},model:{value:(_vm.form.alamat),callback:function ($$v) {_vm.$set(_vm.form, "alamat", $$v)},expression:"form.alamat"}}),_vm._v(" "),_c('b-form-invalid-feedback',{attrs:{"state":_vm.validation.alamat}},[_vm._v("\n                    Alamat tidak valid\n                ")])],1)],1),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"col-md-4\" data-v-2337813a>","</div>",[_c('b-form-group',{attrs:{"id":"input-group-kota","label":"Kota showroom","label-for":"input-kota"}},[_c('b-form-input',{staticClass:"mt-3",attrs:{"id":"input-kota","state":_vm.validation.kota,"placeholder":"Kota showroom"},model:{value:(_vm.form.kota),callback:function ($$v) {_vm.$set(_vm.form, "kota", $$v)},expression:"form.kota"}}),_vm._v(" "),_c('b-form-invalid-feedback',{attrs:{"state":_vm.validation.kota}},[_vm._v("\n                    Kota tidak valid\n                ")])],1)],1),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"col-md-4\" data-v-2337813a>","</div>",[_c('b-form-group',{attrs:{"id":"input-group-droppin","label":"Drop Pin Alamat","label-for":"input-droppin"}},[_c('b-form-input',{staticClass:"mt-3",attrs:{"id":"input-droppin","state":_vm.validation.drop_pin,"placeholder":"Drop Pin google maps"},model:{value:(_vm.form.drop_pin),callback:function ($$v) {_vm.$set(_vm.form, "drop_pin", $$v)},expression:"form.drop_pin"}}),_vm._v(" "),_c('b-form-invalid-feedback',{attrs:{"state":_vm.validation.drop_pin}},[_vm._v("\n                    Drop pin tidak valid\n                ")])],1)],1),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"col-12\" data-v-2337813a>","</div>",[_vm._ssrNode("<div class=\"listbrand-container\" data-v-2337813a>","</div>",[_c('b-form-group',{attrs:{"id":"input-group-listbrand","label":"List Brand","label-for":"input-listbrand"}},[_c('b-form-checkbox-group',{attrs:{"id":"checkbox-group-1","options":_vm.listBrands,"state":_vm.validation.list_brand,"stacked":""},model:{value:(_vm.form.list_brand),callback:function ($$v) {_vm.$set(_vm.form, "list_brand", $$v)},expression:"form.list_brand"}}),_vm._v(" "),_c('b-form-invalid-feedback',{attrs:{"state":_vm.validation.list_brand}},[_vm._v("\n                        Pilih minimal 1 brand\n                    ")])],1)],1)]),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"col-12\" data-v-2337813a>","</div>",[_c('b-form-group',{attrs:{"id":"input-group-wacontact","label":"No. WA Contact","label-for":"input-wacontact"}},[_c('b-form-input',{staticClass:"mt-3",attrs:{"id":"input-wacontact","placeholder":"contoh: 082248687953","state":_vm.validation.wa_contact},model:{value:(_vm.form.wa_contact),callback:function ($$v) {_vm.$set(_vm.form, "wa_contact", $$v)},expression:"form.wa_contact"}}),_vm._v(" "),_c('b-form-invalid-feedback',{attrs:{"state":_vm.validation.wa_contact}},[_vm._v("\n                    Nomor WA tidak valid\n                ")])],1)],1),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"col\" data-v-2337813a>","</div>",[(_vm.type == 'insert')?_c('b-button',{staticClass:"px-5",attrs:{"variant":"primary","pill":"","disabled":_vm.loading},on:{"click":_vm.handleInsert}},[_vm._v("Add Showroom")]):_c('b-button',{staticClass:"px-5",attrs:{"variant":"primary","pill":"","disabled":_vm.loading},on:{"click":_vm.handleUpdate}},[_vm._v("Update Showroom")])],1)],2)],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/home/mutations/showroom-form.vue?vue&type=template&id=2337813a&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/home/mutations/showroom-form.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var showroom_formvue_type_script_lang_js_ = ({
  data() {
    return {
      type: 'insert',
      loading: false,
      form: {
        title: '',
        alamat: '',
        list_brand: [],
        kota: '',
        drop_pin: '',
        wa_contact: '',
        status: 0
      },
      listBrands: [],
      validation: {
        title: null,
        alamat: null,
        list_brand: null,
        kota: null,
        drop_pin: null,
        wa_contact: null,
        status: null
      }
    };
  },

  methods: {
    async handleInsert() {
      this.loading = true;

      for (const key in this.validation) {
        this.validation[key] = null;
      }

      await this.$axios.post(`/showrooms/add`, this.form, {
        headers: {
          "auth-token": this.$cookies.get('admin-token')
        }
      }).then(res => {
        this.$router.push({
          name: 'home-showrooms'
        });
      }).catch(err => {
        err.response.data.errors.forEach(row => {
          this.validation[row.param] = false;
        });
      });
      this.loading = false;
    },

    async handleUpdate() {
      this.loading = true;

      for (const key in this.validation) {
        this.validation[key] = null;
      }

      await this.$axios.put(`/showrooms/update`, this.form, {
        headers: {
          "auth-token": this.$cookies.get('admin-token')
        }
      }).then(res => {
        this.$router.push({
          name: 'home-showroom'
        });
      }).catch(err => {
        err.response.data.errors.forEach(row => {
          this.validation[row.param] = false;
        });
      });
      this.loading = false;
    }

  },

  async mounted() {
    let fetchBrands = await this.$axios.get(`/user/partners`);
    fetchBrands.data.data.forEach(row => {
      this.listBrands.push({
        text: row.nama_perusahaan,
        value: row.id_user
      });
    });

    if (localStorage.getItem('payload-edit')) {
      this.type = 'update';
      let row = JSON.parse(localStorage.getItem('payload-edit'));
      let tmpBrandId = [];
      row.list_brand.forEach(b => {
        tmpBrandId.push(b.id);
      });
      row.list_brand = tmpBrandId;
      this.form = row;
    }
  }

});
// CONCATENATED MODULE: ./pages/home/mutations/showroom-form.vue?vue&type=script&lang=js&
 /* harmony default export */ var mutations_showroom_formvue_type_script_lang_js_ = (showroom_formvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./pages/home/mutations/showroom-form.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(46)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  mutations_showroom_formvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "2337813a",
  "80e615f2"
  
)

/* harmony default export */ var showroom_form = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=showroom-form.js.map