exports.ids = [10];
exports.modules = {

/***/ 57:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/home/mutations/artikel-form.vue?vue&type=template&id=ac96e640&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container-fluid mt-4"},[_vm._ssrNode("<div class=\"row\"><div class=\"col-12\"><h1>Artikel Form</h1></div></div> "),_vm._ssrNode("<div class=\"row\">","</div>",[_vm._ssrNode("<div class=\"col-md-6\">","</div>",[_c('b-form-group',{attrs:{"id":"input-group-title","label":"Judul Artikel","label-for":"input-title"}},[_c('b-form-input',{attrs:{"id":"input-title","placeholder":"Masukkan judul artikel","state":_vm.validation.title},model:{value:(_vm.form.title),callback:function ($$v) {_vm.$set(_vm.form, "title", $$v)},expression:"form.title"}}),_vm._v(" "),_c('b-form-invalid-feedback',{attrs:{"state":_vm.validation.title}},[_vm._v("\n                    Judul tidak valid\n                ")])],1)],1),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"col-12\">","</div>",[_c('b-form-group',{attrs:{"id":"input-group-body","label":"Konten Artikel","label-for":"input-body"}},[_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-md-12 mb-1"},[_c('b-button-group',[_c('b-button',{attrs:{"variant":"primary"}},[_c('b-icon',{attrs:{"icon":"type-bold"},on:{"click":function($event){return _vm.wrapSelection('**')}}})],1),_vm._v(" "),_c('b-button',{attrs:{"variant":"primary"}},[_c('b-icon',{attrs:{"icon":"type-italic"},on:{"click":function($event){return _vm.wrapSelection('*')}}})],1),_vm._v(" "),_c('b-button',{attrs:{"variant":"primary"}},[_c('b-icon',{attrs:{"icon":"dash"},on:{"click":function($event){return _vm.appendSelection('\n***')}}})],1),_vm._v(" "),_c('b-button',{attrs:{"variant":"primary"}},[_c('b-icon',{attrs:{"icon":"type-strikethrough"},on:{"click":function($event){return _vm.wrapSelection('~~')}}})],1)],1),_vm._v(" "),_c('b-button-group',[_c('b-button',{attrs:{"variant":"primary"}},[_c('b-icon',{attrs:{"icon":"link45deg"},on:{"click":function($event){_vm.appendSelection('[link](https://www.example.com)')}}})],1),_vm._v(" "),_c('b-button',{attrs:{"variant":"primary"}},[_c('b-icon',{attrs:{"icon":"card-image"},on:{"click":function($event){_vm.appendSelection('![alt text](image.jpg)')}}})],1),_vm._v(" "),_c('b-button',{attrs:{"variant":"primary"}},[_c('b-icon',{attrs:{"icon":"list-ol"},on:{"click":function($event){return _vm.appendSelection('\n1. \n2. \n3. \n')}}})],1),_vm._v(" "),_c('b-button',{attrs:{"variant":"primary"}},[_c('b-icon',{attrs:{"icon":"list-ul"},on:{"click":function($event){return _vm.appendSelection('\n- \n- \n- \n')}}})],1)],1)],1),_vm._v(" "),_c('div',{staticClass:"col-md-6"},[_c('b-button-group',[_c('b-button',{attrs:{"variant":"primary"},on:{"click":function($event){return _vm.appendSelection('# ')}}},[_vm._v("\n                                H1\n                            ")]),_vm._v(" "),_c('b-button',{attrs:{"variant":"primary"},on:{"click":function($event){return _vm.appendSelection('## ')}}},[_vm._v("\n                                H2\n                            ")]),_vm._v(" "),_c('b-button',{attrs:{"variant":"primary"},on:{"click":function($event){return _vm.appendSelection('### ')}}},[_vm._v("\n                                H3\n                            ")]),_vm._v(" "),_c('b-button',{attrs:{"variant":"primary"},on:{"click":function($event){return _vm.appendSelection('#### ')}}},[_vm._v("\n                                H4\n                            ")]),_vm._v(" "),_c('b-button',{attrs:{"variant":"primary"},on:{"click":function($event){return _vm.appendSelection('##### ')}}},[_vm._v("\n                                H5\n                            ")]),_vm._v(" "),_c('b-button',{attrs:{"variant":"primary"},on:{"click":function($event){return _vm.appendSelection('###### ')}}},[_vm._v("\n                                H6\n                            ")])],1)],1)]),_vm._v(" "),_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-md-6"},[_c('b-form-textarea',{ref:"md-textarea",staticClass:"mt-3",attrs:{"id":"input-body","placeholder":"Enter something...","rows":"8","max-rows":"8"},model:{value:(_vm.form.body),callback:function ($$v) {_vm.$set(_vm.form, "body", $$v)},expression:"form.body"}}),_vm._v(" "),_c('b-form-invalid-feedback',{attrs:{"state":_vm.validation.body}},[_vm._v("\n                            Konten tidak boleh kosong\n                        ")])],1),_vm._v(" "),_c('div',{staticClass:"col-md-6"},[_c('h6',[_vm._v("Result")]),_vm._v(" "),_c('div',{staticClass:"text-break",domProps:{"innerHTML":_vm._s(_vm.$md.render(_vm.form.body))}})])])])],1),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"col-md-6\">","</div>",[_c('b-form-group',{attrs:{"id":"input-group-category","label":"Kategori","label-for":"input-category"}},[_c('b-form-input',{attrs:{"id":"input-category","placeholder":"Masukkan kategori","state":_vm.validation.category},model:{value:(_vm.form.category),callback:function ($$v) {_vm.$set(_vm.form, "category", $$v)},expression:"form.category"}}),_vm._v(" "),_c('b-form-invalid-feedback',{attrs:{"state":_vm.validation.category}},[_vm._v("\n                    Masukkan kategori\n                ")])],1)],1),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"col-md-6\">","</div>",[_c('b-form-group',{attrs:{"id":"input-group-tipe","label":"Tipe Artikel","label-for":"input-tipe"}},[_c('b-form-select',{attrs:{"options":['ARTIKEL','FAQ'],"state":_vm.validation.tipe},model:{value:(_vm.form.tipe),callback:function ($$v) {_vm.$set(_vm.form, "tipe", $$v)},expression:"form.tipe"}}),_vm._v(" "),_c('b-form-invalid-feedback',{attrs:{"state":_vm.validation.tipe}},[_vm._v("\n                    Pilih Tipe Artikel\n                ")])],1)],1),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"col-md-6\">","</div>",[_c('b-form-group',{attrs:{"id":"input-group-author","label":"Ditulis oleh","label-for":"input-author"}},[_c('b-form-select',{attrs:{"options":_vm.listAuthor,"state":_vm.validation.author},model:{value:(_vm.form.author),callback:function ($$v) {_vm.$set(_vm.form, "author", $$v)},expression:"form.author"}}),_vm._v(" "),_c('b-form-invalid-feedback',{attrs:{"state":_vm.validation.author}},[_vm._v("\n                    Pilih author\n                ")])],1)],1),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"col-12\">","</div>",[(_vm.type == 'insert')?_c('b-button',{staticClass:"px-5",attrs:{"variant":"primary","pill":"","disabled":_vm.loading},on:{"click":_vm.handleInsert}},[_vm._v("Add Artikel")]):_c('b-button',{staticClass:"px-5",attrs:{"variant":"primary","pill":"","disabled":_vm.loading},on:{"click":_vm.handleUpdate}},[_vm._v("Update Artikel")])],1)],2)],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/home/mutations/artikel-form.vue?vue&type=template&id=ac96e640&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/home/mutations/artikel-form.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var artikel_formvue_type_script_lang_js_ = ({
  data() {
    return {
      type: 'insert',
      loading: false,
      listAuthor: ['Mitrapabrik', 'Ko Hendra', 'Pak Johan'],
      customButtons: [{
        label: "H1",
        key: "h1",
        icon: "fas fa-h1",
        group: "basic"
      }],
      form: {
        title: '',
        body: '',
        category: '',
        author: '',
        tipe: ''
      },
      validation: {
        title: null,
        body: null,
        category: null,
        author: null,
        tipe: null
      }
    };
  },

  methods: {
    async handleInsert() {
      this.loading = true;

      for (const key in this.validation) {
        this.validation[key] = null;
      }

      await this.$axios.post(`/artikels`, this.form, {
        headers: {
          "auth-token": this.$cookies.get('admin-token')
        }
      }).then(res => {
        this.$router.push({
          name: 'home-artikels'
        });
      }).catch(err => {
        err.response.data.errors.forEach(row => {
          this.validation[row.param] = false;
        });
      });
      this.loading = false;
    },

    async handleUpdate() {
      this.loading = true;

      for (const key in this.validation) {
        this.validation[key] = null;
      }

      await this.$axios.put(`/artikels`, this.form, {
        headers: {
          "auth-token": this.$cookies.get('admin-token')
        }
      }).then(res => {
        this.$router.push({
          name: 'home-artikels'
        });
      }).catch(err => {
        err.response.data.errors.forEach(row => {
          this.validation[row.param] = false;
        });
      });
      this.loading = false;
    },

    wrapSelection(wrappingString) {
      let textArea = this.$refs["md-textarea"];
      var s = textArea.selectionStart;
      var e = textArea.selectionEnd;
      var oldValue = textArea.value;
      var newValue = oldValue.slice(0, s) + wrappingString + oldValue.slice(s, e) + wrappingString + oldValue.slice(e, oldValue.length);
      this.form.body = newValue;
    },

    appendSelection(wrappingString) {
      let textArea = this.$refs["md-textarea"];
      var s = textArea.selectionStart;
      var e = textArea.selectionEnd;
      var oldValue = textArea.value;
      var newValue = oldValue.slice(0, s) + wrappingString + oldValue.slice(s, e) + oldValue.slice(e, oldValue.length);
      this.form.body = newValue;
    }

  },

  mounted() {
    if (localStorage.getItem('payload-edit')) {
      this.type = 'update';
      let row = JSON.parse(localStorage.getItem('payload-edit'));
      console.log(row);
      this.form = row;
    }
  }

});
// CONCATENATED MODULE: ./pages/home/mutations/artikel-form.vue?vue&type=script&lang=js&
 /* harmony default export */ var mutations_artikel_formvue_type_script_lang_js_ = (artikel_formvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./pages/home/mutations/artikel-form.vue





/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  mutations_artikel_formvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "b77de372"
  
)

/* harmony default export */ var artikel_form = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=artikel-form.js.map