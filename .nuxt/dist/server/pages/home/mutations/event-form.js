exports.ids = [11];
exports.modules = {

/***/ 58:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/home/mutations/event-form.vue?vue&type=template&id=44ee4dc6&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container-fluid mt-4"},[_vm._ssrNode("<div class=\"row\"><div class=\"col-12\"><h1>Event Form</h1></div></div> "),_vm._ssrNode("<div class=\"row\">","</div>",[_vm._ssrNode("<div class=\"col-md-6\">","</div>",[_c('b-form-group',{attrs:{"id":"input-group-title","label":"Judul Event","label-for":"input-title"}},[_c('b-form-input',{attrs:{"id":"input-title","placeholder":"Masukkan judul event","state":_vm.validation.event_name},model:{value:(_vm.form.event_name),callback:function ($$v) {_vm.$set(_vm.form, "event_name", $$v)},expression:"form.event_name"}}),_vm._v(" "),_c('b-form-invalid-feedback',{attrs:{"state":_vm.validation.event_name}},[_vm._v("\n                    Judul event tidak valid\n                ")])],1)],1),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"col-12\">","</div>",[_c('b-form-group',{attrs:{"id":"input-group-body","label":"Konten Event","label-for":"input-body"}},[_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-md-12 mb-1"},[_c('b-button-group',[_c('b-button',{attrs:{"variant":"primary"}},[_c('b-icon',{attrs:{"icon":"type-bold"},on:{"click":function($event){return _vm.wrapSelection('**')}}})],1),_vm._v(" "),_c('b-button',{attrs:{"variant":"primary"}},[_c('b-icon',{attrs:{"icon":"type-italic"},on:{"click":function($event){return _vm.wrapSelection('*')}}})],1),_vm._v(" "),_c('b-button',{attrs:{"variant":"primary"}},[_c('b-icon',{attrs:{"icon":"dash"},on:{"click":function($event){return _vm.appendSelection('\n***')}}})],1),_vm._v(" "),_c('b-button',{attrs:{"variant":"primary"}},[_c('b-icon',{attrs:{"icon":"type-strikethrough"},on:{"click":function($event){return _vm.wrapSelection('~~')}}})],1)],1),_vm._v(" "),_c('b-button-group',[_c('b-button',{attrs:{"variant":"primary"}},[_c('b-icon',{attrs:{"icon":"link45deg"},on:{"click":function($event){_vm.appendSelection('[link](https://www.example.com)')}}})],1),_vm._v(" "),_c('b-button',{attrs:{"variant":"primary"}},[_c('b-icon',{attrs:{"icon":"card-image"},on:{"click":function($event){_vm.appendSelection('![alt text](image.jpg)')}}})],1),_vm._v(" "),_c('b-button',{attrs:{"variant":"primary"}},[_c('b-icon',{attrs:{"icon":"list-ol"},on:{"click":function($event){return _vm.appendSelection('\n1. \n2. \n3. \n')}}})],1),_vm._v(" "),_c('b-button',{attrs:{"variant":"primary"}},[_c('b-icon',{attrs:{"icon":"list-ul"},on:{"click":function($event){return _vm.appendSelection('\n- \n- \n- \n')}}})],1)],1)],1),_vm._v(" "),_c('div',{staticClass:"col-md-6"},[_c('b-button-group',[_c('b-button',{attrs:{"variant":"primary"},on:{"click":function($event){return _vm.appendSelection('# ')}}},[_vm._v("\n                                H1\n                            ")]),_vm._v(" "),_c('b-button',{attrs:{"variant":"primary"},on:{"click":function($event){return _vm.appendSelection('## ')}}},[_vm._v("\n                                H2\n                            ")]),_vm._v(" "),_c('b-button',{attrs:{"variant":"primary"},on:{"click":function($event){return _vm.appendSelection('### ')}}},[_vm._v("\n                                H3\n                            ")]),_vm._v(" "),_c('b-button',{attrs:{"variant":"primary"},on:{"click":function($event){return _vm.appendSelection('#### ')}}},[_vm._v("\n                                H4\n                            ")]),_vm._v(" "),_c('b-button',{attrs:{"variant":"primary"},on:{"click":function($event){return _vm.appendSelection('##### ')}}},[_vm._v("\n                                H5\n                            ")]),_vm._v(" "),_c('b-button',{attrs:{"variant":"primary"},on:{"click":function($event){return _vm.appendSelection('###### ')}}},[_vm._v("\n                                H6\n                            ")])],1)],1)]),_vm._v(" "),_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-md-6"},[_c('b-form-textarea',{ref:"md-textarea",staticClass:"mt-3",attrs:{"id":"input-body","placeholder":"Enter something...","rows":"8","max-rows":"8"},model:{value:(_vm.form.body),callback:function ($$v) {_vm.$set(_vm.form, "body", $$v)},expression:"form.body"}}),_vm._v(" "),_c('b-form-invalid-feedback',{attrs:{"state":_vm.validation.body}},[_vm._v("\n                            Konten tidak boleh kosong\n                        ")])],1),_vm._v(" "),_c('div',{staticClass:"col-md-6"},[_c('h6',[_vm._v("Result")]),_vm._v(" "),_c('div',{staticClass:"text-break",domProps:{"innerHTML":_vm._s(_vm.$md.render(_vm.form.body))}})])])])],1),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"col-6\">","</div>",[_c('b-form-group',{attrs:{"id":"input-group-startdate","label":"Tanggal mulai event","label-for":"input-startdate"}},[_c('b-form-input',{attrs:{"id":"input-startdate","type":"date","state":_vm.validation.starting_date},model:{value:(_vm.form.starting_date),callback:function ($$v) {_vm.$set(_vm.form, "starting_date", $$v)},expression:"form.starting_date"}}),_vm._v(" "),_c('b-form-invalid-feedback',{attrs:{"state":_vm.validation.starting_date}},[_vm._v("\n                    Tentukan tanggal yang valid\n                ")])],1)],1),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"col-6\">","</div>",[_c('b-form-group',{attrs:{"id":"input-group-author","label":"Penyelenggara","label-for":"input-author"}},[_c('b-form-select',{attrs:{"options":_vm.listAuthor,"state":_vm.validation.author},model:{value:(_vm.form.author),callback:function ($$v) {_vm.$set(_vm.form, "author", $$v)},expression:"form.author"}}),_vm._v(" "),_c('b-form-invalid-feedback',{attrs:{"state":_vm.validation.author}},[_vm._v("\n                    Pilih penyelenggara\n                ")])],1)],1),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"col-md-6\">","</div>",[_c('b-form-group',{attrs:{"id":"input-group-location","label":"Lokasi Event","label-for":"input-location"}},[_c('b-form-input',{attrs:{"id":"input-location","placeholder":"Lokasi","state":_vm.validation.location},model:{value:(_vm.form.location),callback:function ($$v) {_vm.$set(_vm.form, "location", $$v)},expression:"form.location"}}),_vm._v(" "),_c('b-form-invalid-feedback',{attrs:{"state":_vm.validation.location}},[_vm._v("\n                    Lokasi tidak valid\n                ")])],1)],1),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"col-md-6\">","</div>",[_c('b-form-group',{attrs:{"id":"input-group-admission","label":"Biaya Pendaftaran","label-for":"input-admission"}},[_c('b-form-input',{attrs:{"id":"input-admission","placeholder":"Masukkan biaya pendaftaran","state":_vm.validation.admission},model:{value:(_vm.form.admission),callback:function ($$v) {_vm.$set(_vm.form, "admission", $$v)},expression:"form.admission"}}),_vm._v(" "),_c('b-form-invalid-feedback',{attrs:{"state":_vm.validation.admission}},[_vm._v("\n                    Biaya pendaftaran tidak valid\n                ")])],1)],1),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"col\">","</div>",[(_vm.type == 'insert')?_c('b-button',{staticClass:"px-5",attrs:{"variant":"primary","pill":"","disabled":_vm.loading},on:{"click":_vm.handleInsert}},[_vm._v("Add Event")]):_c('b-button',{staticClass:"px-5",attrs:{"variant":"primary","pill":"","disabled":_vm.loading},on:{"click":_vm.handleUpdate}},[_vm._v("Update Event")])],1)],2)],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/home/mutations/event-form.vue?vue&type=template&id=44ee4dc6&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/home/mutations/event-form.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var event_formvue_type_script_lang_js_ = ({
  data() {
    return {
      type: 'insert',
      loading: false,
      listCategory: ['Kategori 1', 'Kategori 2', 'Kategori 3'],
      listAuthor: ['Dow Jones', 'Jon Doe', 'Mark Joe'],
      customButtons: [{
        label: "H1",
        key: "h1",
        icon: "fas fa-h1",
        group: "basic"
      }],
      form: {
        event_name: '',
        body: '',
        starting_date: '',
        author: '',
        location: '',
        admission: ''
      },
      validation: {
        event_name: null,
        body: null,
        starting_date: null,
        author: null,
        location: null,
        admission: null
      }
    };
  },

  methods: {
    async handleInsert() {
      this.loading = true;

      for (const key in this.validation) {
        this.validation[key] = null;
      }

      await this.$axios.post(`/events`, this.form, {
        headers: {
          "auth-token": this.$cookies.get('admin-token')
        }
      }).then(res => {
        this.$router.push({
          name: 'home-events'
        });
      }).catch(err => {
        err.response.data.errors.forEach(row => {
          this.validation[row.param] = false;
        });
      });
      this.loading = false;
    },

    async handleUpdate() {
      this.loading = true;

      for (const key in this.validation) {
        this.validation[key] = null;
      }

      await this.$axios.put(`/events`, this.form, {
        headers: {
          "auth-token": this.$cookies.get('admin-token')
        }
      }).then(res => {
        this.$router.push({
          name: 'home-events'
        });
      }).catch(err => {
        err.response.data.errors.forEach(row => {
          this.validation[row.param] = false;
        });
      });
      this.loading = false;
    },

    wrapSelection(wrappingString) {
      let textArea = this.$refs["md-textarea"];
      var s = textArea.selectionStart;
      var e = textArea.selectionEnd;
      var oldValue = textArea.value;
      var newValue = oldValue.slice(0, s) + wrappingString + oldValue.slice(s, e) + wrappingString + oldValue.slice(e, oldValue.length);
      this.form.body = newValue;
    },

    appendSelection(wrappingString) {
      let textArea = this.$refs["md-textarea"];
      var s = textArea.selectionStart;
      var e = textArea.selectionEnd;
      var oldValue = textArea.value;
      var newValue = oldValue.slice(0, s) + wrappingString + oldValue.slice(s, e) + oldValue.slice(e, oldValue.length);
      this.form.body = newValue;
    }

  },

  mounted() {
    if (localStorage.getItem('payload-edit')) {
      this.type = 'update';
      let row = JSON.parse(localStorage.getItem('payload-edit'));
      console.log(row);
      this.form = row;
    }
  }

});
// CONCATENATED MODULE: ./pages/home/mutations/event-form.vue?vue&type=script&lang=js&
 /* harmony default export */ var mutations_event_formvue_type_script_lang_js_ = (event_formvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./pages/home/mutations/event-form.vue





/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  mutations_event_formvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "b8c497ce"
  
)

/* harmony default export */ var event_form = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=event-form.js.map