exports.ids = [6];
exports.modules = {

/***/ 54:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/home/dashboard.vue?vue&type=template&id=399f30bc&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container-fluid"},[_vm._ssrNode("<div class=\"row mt-3\" data-v-399f30bc><div class=\"col-12\" data-v-399f30bc><h1 data-v-399f30bc>Dashboard</h1> <h1 data-v-399f30bc>Hello Analytics Reporting API V4</h1></div> <div class=\"col-12\" data-v-399f30bc><p class=\"g-signin2\" data-v-399f30bc></p> <textarea cols=\"80\" rows=\"20\" id=\"query-output\" data-v-399f30bc></textarea></div></div>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/home/dashboard.vue?vue&type=template&id=399f30bc&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/home/dashboard.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var dashboardvue_type_script_lang_js_ = ({
  data() {
    return {};
  },

  head() {
    return {
      script: [{
        src: 'https://apis.google.com/js/client:platform.js'
      }],
      meta: [{
        hid: 'google-signin-client_id',
        name: 'google-signin-client_id',
        content: '586582848377-78tqafmm8sur8mn51pt2nb4m1uuuh1ha.apps.googleusercontent.com'
      }, {
        hid: 'google-signin-scope',
        name: 'google-signin-scope',
        content: 'https://www.googleapis.com/auth/analytics.readonly'
      }]
    };
  },

  methods: {
    queryReports() {
      gapi.client.request({
        path: '/v4/reports:batchGet',
        root: 'https://analyticsreporting.googleapis.com/',
        method: 'POST',
        body: {
          reportRequests: [{
            viewId: '256515210',
            dateRanges: [{
              startDate: '7daysAgo',
              endDate: 'today'
            }],
            metrics: [{
              expression: 'ga:sessions'
            }]
          }]
        }
      }).then(this.displayResults, console.error.bind(console));
    },

    displayResults(response) {
      var formattedJson = JSON.stringify(response.result, null, 2);
      document.getElementById('query-output').value = formattedJson;
    }

  },

  mounted() {// Replace with your view ID.
    // var VIEW_ID = '256515210';
    // this.queryReports(VIEW_ID)
  }

});
// CONCATENATED MODULE: ./pages/home/dashboard.vue?vue&type=script&lang=js&
 /* harmony default export */ var home_dashboardvue_type_script_lang_js_ = (dashboardvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./pages/home/dashboard.vue



function injectStyles (context) {
  
  
}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  home_dashboardvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "399f30bc",
  "74de0f18"
  
)

/* harmony default export */ var dashboard = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=dashboard.js.map