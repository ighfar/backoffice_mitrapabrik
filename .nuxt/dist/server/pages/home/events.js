exports.ids = [8];
exports.modules = {

/***/ 55:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/home/events.vue?vue&type=template&id=9990dee8&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container-fluid mx-4"},[_vm._ssrNode("<div class=\"row mt-3\" data-v-9990dee8><div class=\"col-12\" data-v-9990dee8><h1 data-v-9990dee8>Events</h1></div></div> "),_vm._ssrNode("<div class=\"row\" data-v-9990dee8>","</div>",[_vm._ssrNode("<div class=\"col-12 mb-3\" data-v-9990dee8>","</div>",[_c('b-button',{staticClass:"px-5",attrs:{"variant":"primary","pill":"","size":"sm"},on:{"click":_vm.openInsertForm}},[_vm._v("+ Add")])],1),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"col-12 p-0\" data-v-9990dee8>","</div>",[(!_vm.loading)?_c('b-table',{attrs:{"items":_vm.items,"fields":_vm.fields,"responsive":"","head-variant":"dark","current-page":_vm.currentPage,"per-page":_vm.perPage,"filter":_vm.filter},on:{"filtered":_vm.onFiltered},scopedSlots:_vm._u([{key:"cell(starting_date)",fn:function(row){return [_vm._v("\n                    "+_vm._s(_vm.$formatIDDate(row.item.starting_date))+"\n                ")]}},{key:"cell(status)",fn:function(row){return [_vm._v("\n                    "+_vm._s(row.item.status == 0 ? 'Unpublished.' : 'Published.')+"\n                ")]}},{key:"cell(action)",fn:function(row){return [_c('b-button-group',[_c('b-button',{staticClass:"mb-2 px-4",attrs:{"size":"sm","variant":"success","pill":""},on:{"click":function($event){return _vm.publishEvent(row)}}},[_vm._v(_vm._s(row.item.status == 0 ? 'Publish' : 'Unpublish'))]),_vm._v(" "),_c('b-button',{staticClass:"mb-2 px-4",attrs:{"size":"sm","variant":"primary","pill":""},on:{"click":function($event){return _vm.openEditForm(row)}}},[_vm._v("Edit")]),_vm._v(" "),_c('b-button',{staticClass:"mb-2 px-4",attrs:{"size":"sm","variant":"dark","pill":""},on:{"click":function($event){return _vm.openDeleteAlert(row)}}},[_vm._v("Delete")]),_vm._v(" "),_c('nuxt-link',{attrs:{"to":("/home/event/rsvp?id=" + (row.item.id_event))}},[_c('b-button',{staticClass:"mb-2 px-4",attrs:{"size":"sm","variant":"info","pill":""}},[_vm._v("RSVP List")])],1)],1)]}}],null,false,2931784070)}):_vm._e()],1),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"col-12\" data-v-9990dee8>","</div>",[(!_vm.loading)?_c('b-pagination',{staticClass:"my-3",attrs:{"total-rows":_vm.totalRows,"per-page":_vm.perPage,"align":"right","size":"sm"},model:{value:(_vm.currentPage),callback:function ($$v) {_vm.currentPage=$$v},expression:"currentPage"}}):_vm._e()],1)],2),_vm._ssrNode(" "),_c('b-modal',{attrs:{"centered":"","content-class":"shadow","visible":_vm.showAlert,"hide-footer":"","hide-header":""}},[_c('p',{staticClass:"my-2 text-center"},[_vm._v("Apakah anda yakin hendak menghapus data ini?")]),_vm._v(" "),_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-md-12"},[_c('b-button',{staticClass:"w-25 float-right mx-2",attrs:{"variant":"primary","pill":""},on:{"click":_vm.deleteData}},[_vm._v("Ya")]),_vm._v(" "),_c('b-button',{staticClass:"w-25 float-right mx-2",attrs:{"variant":"light","pill":""},on:{"click":function($event){_vm.showAlert = !_vm.showAlert}}},[_vm._v("Tidak")])],1)])])],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/home/events.vue?vue&type=template&id=9990dee8&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/home/events.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var eventsvue_type_script_lang_js_ = ({
  data() {
    return {
      showAlert: false,
      loading: null,
      items: [],
      fields: [{
        key: 'id_event',
        sortable: true
      }, {
        key: 'event_name',
        sortable: true
      }, {
        key: 'starting_date',
        sortable: true
      }, {
        key: 'author',
        sortable: true
      }, {
        key: 'location',
        sortable: true
      }, {
        key: 'admission',
        sortable: true
      }, {
        key: 'status',
        sortable: true
      }, {
        key: 'action',
        sortable: false
      }],
      currentPage: 1,
      totalRows: 0,
      perPage: 10,
      filter: '',
      tmpData: {}
    };
  },

  methods: {
    async fetchTableData() {
      let listEvents = await this.$axios.get(`/events`);
      this.totalRows = listEvents.data.data.length;
      this.items = listEvents.data.data;
    },

    onFiltered(filteredItems) {
      this.totalRows = filteredItems.length;
      this.currentPage = 1;
    },

    openInsertForm() {
      localStorage.removeItem('payload-edit');
      this.$router.push({
        name: 'home-mutations-event-form'
      });
    },

    openEditForm(row) {
      row.item.starting_date = this.$formatDate(row.item.starting_date);
      localStorage.setItem('payload-edit', JSON.stringify(row.item));
      this.$router.push({
        name: 'home-mutations-event-form'
      });
    },

    openDeleteAlert(row) {
      this.showAlert = true;
      this.tmpData = row;
    },

    async publishEvent(row) {
      await this.$axios.put(`/events/publish`, {
        id_event: row.item.id_event,
        status: row.item.status == 1 ? 0 : 1
      }, {
        headers: {
          "auth-token": this.$cookies.get('admin-token')
        }
      }).then(async res => {
        this.$bvToast.toast(`Event status updated.`, {
          title: 'Info',
          autoHideDelay: 5000,
          solid: true
        });
        await this.fetchTableData();
      }).catch(err => {
        this.$bvToast.toast(`Mohon maaf terjadi kesalahan. Silahkan coba beberapa saat lagi.`, {
          title: 'Error',
          autoHideDelay: 5000,
          solid: true
        });
      });
    },

    async deleteData() {
      await this.$axios.delete(`/events`, {
        headers: {
          "auth-token": this.$cookies.get('admin-token')
        },
        data: {
          id_event: this.tmpData.item.id_event
        }
      }).then(async res => {
        this.$bvToast.toast(`Event deleted.`, {
          title: 'Info',
          autoHideDelay: 5000,
          solid: true
        });
        await this.fetchTableData();
      }).catch(err => {
        console.log(err.response);
      });
      this.showAlert = false;
    }

  },

  async mounted() {
    await this.fetchTableData();
    this.loading = false;
  }

});
// CONCATENATED MODULE: ./pages/home/events.vue?vue&type=script&lang=js&
 /* harmony default export */ var home_eventsvue_type_script_lang_js_ = (eventsvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./pages/home/events.vue



function injectStyles (context) {
  
  
}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  home_eventsvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "9990dee8",
  "e332422e"
  
)

/* harmony default export */ var events = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=events.js.map