import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _e9a820d8 = () => interopDefault(import('..\\pages\\home.vue' /* webpackChunkName: "pages/home" */))
const _44ced05b = () => interopDefault(import('..\\pages\\home\\artikels.vue' /* webpackChunkName: "pages/home/artikels" */))
const _6969d917 = () => interopDefault(import('..\\pages\\home\\assets.vue' /* webpackChunkName: "pages/home/assets" */))
const _532fae71 = () => interopDefault(import('..\\pages\\home\\customers.vue' /* webpackChunkName: "pages/home/customers" */))
const _af8635a0 = () => interopDefault(import('..\\pages\\home\\dashboard.vue' /* webpackChunkName: "pages/home/dashboard" */))
const _25248bad = () => interopDefault(import('..\\pages\\home\\events.vue' /* webpackChunkName: "pages/home/events" */))
const _67bba286 = () => interopDefault(import('..\\pages\\home\\mutations.vue' /* webpackChunkName: "pages/home/mutations" */))
const _152c8acb = () => interopDefault(import('..\\pages\\home\\mutations\\artikel-form.vue' /* webpackChunkName: "pages/home/mutations/artikel-form" */))
const _6b86819d = () => interopDefault(import('..\\pages\\home\\mutations\\event-form.vue' /* webpackChunkName: "pages/home/mutations/event-form" */))
const _6c110903 = () => interopDefault(import('..\\pages\\home\\mutations\\showroom-form.vue' /* webpackChunkName: "pages/home/mutations/showroom-form" */))
const _f47a0ee6 = () => interopDefault(import('..\\pages\\home\\scan-qr.vue' /* webpackChunkName: "pages/home/scan-qr" */))
const _419cd96c = () => interopDefault(import('..\\pages\\home\\showroom.vue' /* webpackChunkName: "pages/home/showroom" */))
const _d454971e = () => interopDefault(import('..\\pages\\home\\event\\rsvp.vue' /* webpackChunkName: "pages/home/event/rsvp" */))
const _5bb74f7f = () => interopDefault(import('..\\pages\\logout.vue' /* webpackChunkName: "pages/logout" */))
const _2e8f466d = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/home",
    component: _e9a820d8,
    name: "home",
    children: [{
      path: "artikels",
      component: _44ced05b,
      name: "home-artikels"
    }, {
      path: "assets",
      component: _6969d917,
      name: "home-assets"
    }, {
      path: "customers",
      component: _532fae71,
      name: "home-customers"
    }, {
      path: "dashboard",
      component: _af8635a0,
      name: "home-dashboard"
    }, {
      path: "events",
      component: _25248bad,
      name: "home-events"
    }, {
      path: "mutations",
      component: _67bba286,
      name: "home-mutations",
      children: [{
        path: "artikel-form",
        component: _152c8acb,
        name: "home-mutations-artikel-form"
      }, {
        path: "event-form",
        component: _6b86819d,
        name: "home-mutations-event-form"
      }, {
        path: "showroom-form",
        component: _6c110903,
        name: "home-mutations-showroom-form"
      }]
    }, {
      path: "scan-qr",
      component: _f47a0ee6,
      name: "home-scan-qr"
    }, {
      path: "showroom",
      component: _419cd96c,
      name: "home-showroom"
    }, {
      path: "event/rsvp",
      component: _d454971e,
      name: "home-event-rsvp"
    }]
  }, {
    path: "/logout",
    component: _5bb74f7f,
    name: "logout"
  }, {
    path: "/",
    component: _2e8f466d,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
