function numberFormat(no) {
    var str = no + '';
    var ar = [];
    var i  = str.length -1;

    while( i >= 0 ) {
        ar.push( (str[i-2]||'') + (str[i-1]|| '')+ (str[i]|| ''));
        i= i-3;
    }
    return ar.reverse().join(',');  
}

export default ({ app }, inject) => {
    inject('formatCurrency', no => {
        var ar = (+no).toFixed(2).split('.');
        return [numberFormat(ar[0]|0),'.', ar[1]].join('');
    })
}
